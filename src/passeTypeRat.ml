(* Module de la passe de typage *)
module PasseTypeRat : Passe.Passe with type t1 = Ast.AstTds.programme and type t2 = Ast.AstType.programme =
struct

  open Tds
  open Exceptions
  open Ast
  open AstType
  open Type

  type t1 = Ast.AstTds.programme
  type t2 = Ast.AstType.programme


(*
  analyse_type_affectable:
    AstTds.affectable -> affectable * typ
  Description:
    Vérifie le bon typage d'un affectable
  Parameters:
    - a : affectable a analyser
  Returns:
    Le nouvel affectable et son type
*)
let rec analyse_type_affectable a =
  match a with
  | AstTds.Dref(a) ->
    begin
      match (analyse_type_affectable a) with
      | (na, Pointeur t) -> (AstType.Dref(na, t), t)
      | _ -> failwith "Internal Error"
    end
  
  | AstTds.Ident(info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, t, _, _) -> (AstType.Ident(info), t)
      | InfoConst _ -> (AstType.Ident(info), Int)
      | _ -> failwith "Internal Error"
    end
  
  | AstTds.Attribut(aff, info) ->
    begin
      let (n_aff, _) = analyse_type_affectable aff in
      match info_ast_to_info info with
      | InfoVar(_, t, _, _) ->
        (AstType.Attribut(n_aff, info), t)
      | _ -> failwith "Internal Error"
    end
  

(*
  analyse_type_expression:
    AstTds.expression -> AstType.expression * typ
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme l'expression en une expression de type AstType.expression
  Parameters:
    - e : l'expression à analyser
  Returns:
    Une nouvelle AstType.expression
  Exceptions:
    - TypesParametresInattendus
    - TypeInattendu
    - TypeBinaireInattendu
*)
let rec analyse_type_expression e =
  match e with
  | AstTds.AppelFonction(info, l_expr) ->
    let n_l_expr, l_type = List.split (List.map analyse_type_expression l_expr) in
    begin
      match info_ast_to_info info with
      | InfoFun(_, t, l_type_fun) ->
        if (est_compatible_list l_type_fun l_type) then
          (AstType.AppelFonction(info, n_l_expr), t)
        else (* Les types mis en paramètre ne correpondent pas au types demandés *)
          raise (TypesParametresInattendus(l_type_fun, l_type))
      | _ -> failwith "Internal Error"
    end

  | AstTds.Unaire(u, expr) ->
    begin
      match (analyse_type_expression expr) with
      | n_expr, Rat ->
        begin
          match u with
          | AstSyntax.Numerateur -> (AstType.Unaire(Numerateur, n_expr), Int)
          | AstSyntax.Denominateur -> (AstType.Unaire(Denominateur, n_expr), Int)
        end
      | _, t -> (* Il n'y a pas d'opérateur unaire autre que ceux sur les rat *)
        raise (TypeInattendu(t, Rat))
    end

  | AstTds.Binaire(b, expr_1, expr_2) ->
    let (n_expr_1, t1) = analyse_type_expression expr_1 in
    let (n_expr_2, t2) = analyse_type_expression expr_2 in
    begin
      match (b, t1, t2) with
      | Fraction, Int, Int -> (AstType.Binaire(Fraction, n_expr_1, n_expr_2), Rat)
      | Plus, Int, Int -> (AstType.Binaire(PlusInt, n_expr_1, n_expr_2), Int)
      | Mult, Int, Int -> (AstType.Binaire(MultInt, n_expr_1, n_expr_2), Int)
      | Equ, Int ,Int -> (AstType.Binaire(EquInt, n_expr_1, n_expr_2), Bool)
      | Inf, Int, Int -> (AstType.Binaire(Inf, n_expr_1, n_expr_2), Bool)
      | Plus, Rat, Rat -> (AstType.Binaire(PlusRat, n_expr_1, n_expr_2), Rat)
      | Mult, Rat, Rat -> (AstType.Binaire(MultRat, n_expr_1, n_expr_2), Rat)
      | Equ, Bool ,Bool -> (AstType.Binaire(EquBool, n_expr_1, n_expr_2), Bool)
      | _, _, _ -> (* Opérations avec erreur de type *)
        raise (TypeBinaireInattendu(b, t1, t2))
    end

  | AstTds.Booleen(b) -> (AstType.Booleen(b), Bool)

  | AstTds.Entier(i) -> (AstType.Entier(i), Int)

  | AstTds.Affectable(a) ->
    let (n_a, n_t) = analyse_type_affectable a in
    (AstType.Affectable(n_a), n_t)
  
  | AstTds.Null -> (AstType.Null, Pointeur(Undefined))

  | AstTds.NewType(t) -> 
    (AstType.NewType(t), Pointeur(t))
  
  | AstTds.Adresse(info) -> 
    begin
      match info_ast_to_info info with
      | InfoVar(_, t, _, _) -> 
        let _ = modifier_type_info (Pointeur(t)) info in
        (AstType.Adresse(info), Pointeur(t))
      | _ -> failwith "Internal Error"
    end
  
  | AstTds.Tuple(l_expr) ->
    let n_l_expr, l_type = List.split (List.map analyse_type_expression l_expr) in
    (AstType.Tuple(n_l_expr), Struct(List.map (fun e -> (e, "")) l_type))


(*
  analyse_type_instruction:
    typ option -> AstTds.instruction -> AstType.instruction
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme l'instruction en une instruction de type AstType.instruction
  Parameters:
    - opt : permet de savoir si on est dans le main, ou dans une fonction
    - i : l'instruction à analyser
  Returns:
    Une nouvelle AstType.instruction
  Exceptions:
    - TypeInattendu
*)
let rec analyse_type_instruction opt i =
  match i with
  | AstTds.Declaration (t, info, e) ->
    let (ne, nt) = analyse_type_expression e in
    if (est_compatible nt t) then
      let _ = modifier_type_info t info in
      AstType.Declaration(info, ne)
    else
      raise (TypeInattendu(nt, t))
    
  | AstTds.Affectation (aff, e) ->
    let (ne, nt) = analyse_type_expression e in
    begin
      match aff with
      | Dref a ->
        begin
          match (analyse_type_affectable a) with
          | (na, Pointeur _) -> AstType.Affectation(na, ne)
          | _ -> failwith "Internal Error"
        end
      
      | AstTds.Ident info ->
        begin
          match (info_ast_to_info info) with
          | InfoVar(_, t, _, _) ->
            if est_compatible t nt then  
              AstType.Affectation(AstType.Ident(info), ne)
            else
              raise (TypeInattendu(nt, t))
          | _ -> failwith "Internal Error"
        end
      
      | AstTds.Attribut(aff2, info) ->
        begin
          match info_ast_to_info info with
          | InfoVar(_, t, _, _) ->
            if est_compatible t nt then  
              let na, _ = analyse_type_affectable aff2 in
              AstType.Affectation(Attribut(na, info), ne)
            else
              raise (TypeInattendu(nt, t))
          | _ -> failwith "Internal Error"
        end
    end
        
  | AstTds.Affichage e ->
    let (ne, nt) = analyse_type_expression e in
    begin
      match nt with
      | Int -> AstType.AffichageInt(ne)
      | Rat -> AstType.AffichageRat(ne)
      | Bool -> AstType.AffichageBool(ne)
      | Pointeur _ -> AstType.AffichagePointeur(ne)
      | Struct _ -> AstType.AffichageStruct(ne, nt)
      | _ -> failwith "Internal Error"
    end
    
  | AstTds.Conditionnelle (e, b1, b2) ->
    let (ne, nt) = analyse_type_expression e in
    let nb1 = analyse_type_bloc opt b1 in
    let nb2 = analyse_type_bloc opt b2 in
    if (est_compatible Bool nt) then
      AstType.Conditionnelle(ne, nb1, nb2)
    else
      raise (TypeInattendu(nt, Bool))

  | AstTds.TantQue (e, b) ->
    let (ne, nt) = analyse_type_expression e in
    if (est_compatible nt Bool) then
      let nb = analyse_type_bloc opt b in
      AstType.TantQue(ne, nb)
    else
      raise (TypeInattendu(nt, Bool))
    
  | AstTds.Retour (e) ->
    begin
      match opt with
      | Some(t) ->
        let (ne, nt) = analyse_type_expression e in
        if est_compatible t nt then
          AstType.Retour(ne)
        else
          raise (TypeInattendu(nt, t))
      | None -> failwith "Internal Error"
    end
    
  | AstTds.Empty -> AstType.Empty


(*
  analyse_type_bloc:
    typ option -> AstTds.bloc -> AstType.bloc
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme le bloc en un bloc de type AstType.bloc
  Parameters:
    - opt : permet de savoir si on est dans le main, ou dans une fonction
    - li : les instructions (bloc) à analyser
  Returns:
    Un nouvel AstType.bloc
*)
and analyse_type_bloc opt li =
  let nli = List.map (analyse_type_instruction opt) li in
  nli

(*
  analyse_type_fonction:
    typ option -> AstTds.fonction -> AstType.fonction
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme la fonction en une fonction de type AstTds.fonction
  Parameters:
    - opt : permet de savoir si on est dans le main, ou dans une fonction
    - f : la fonction à analyser
  Returns:
    Une nouvelle AstTds.instruction
  Exceptions:
    - TypeInattendu
*)
let analyse_type_fonction opt (AstTds.Fonction(t, info, l_typinfo, bloc)) =
  match opt with
  | Some t_ret ->
    if est_compatible t_ret t then
      begin
        let n_l_info = List.map (fun (ti, i) -> modifier_type_info ti i; (ti, i)) l_typinfo in
        let tp, ip = List.split n_l_info in
        let _ = modifier_type_fonction_info t tp info in
        let nb = analyse_type_bloc opt bloc in
        AstType.Fonction(info, ip, nb)
      end
    else
      raise (TypeInattendu(t, t_ret))
  | None -> failwith "Internal Error"

(*
  analyser:
    AstTds.ast -> AstType.ast
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme le programme en un programme de type AstType.ast
  Parameters:
    - p : le programme à analyser
  Returns:
    L'AstType correspondant au programme
*)
let analyser (AstTds.Programme(fonctions, prog)) =
  let nf = List.map (fun (AstTds.Fonction(t, info, l_typinfo, bloc)) ->
          analyse_type_fonction (Some t) (AstTds.Fonction(t, info, l_typinfo, bloc))) fonctions in
  let nb = analyse_type_bloc None prog in
  Programme(nf, nb)

end
