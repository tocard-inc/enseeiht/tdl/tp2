(* Module de la passe de gestion des identifiants *)
module PasseTdsRat : Passe.Passe with type t1 = Ast.AstSyntax.programme and type t2 = Ast.AstTds.programme =
struct

  open Tds
  open Exceptions
  open Ast
  open AstTds
  open Type

  type t1 = Ast.AstSyntax.programme
  type t2 = Ast.AstTds.programme


(*
  recherche_type:
    tds -> typ -> typ
  Description:
    Permet d'obtenir le type d'un typedef
  Parameters:
    - tds : la table des symboles que l'on souhaite cherche
    - t : le type que l'on cherche
  Returns:
    Le type d'un typedef
  Exceptions:
    - IdentifiantNonDeclare
*)
let rec recherche_type tds t =
  match t with
  | TIdent(t_nom) ->
    begin
      match chercherGlobalement tds t_nom with
      | None -> raise (IdentifiantNonDeclare (t_nom))
      | Some i ->
        begin
          match info_ast_to_info i with
          | InfoType(_, t2) -> recherche_type tds t2
          | _ -> failwith "Internal Error"
        end
    end
  | Struct (l_typstr) ->
        let n_l_typstr = (List.map (fun (t, n) -> (recherche_type tds t, n)) l_typstr) in
        Struct n_l_typstr
  | _ -> t


(*
  analyse_tds_affectable:
    tds -> AstSyntax.affectable -> bool -> AstTds.affectable
  Description:
    Transforme les affectable de l'AstSyntax en AstTds
  Parameters:
    - tds : la table des symboles courante
    - a : l'affectable à analyser
    - modif : permet de savoir s'il sagit d'un affectable dans une expression ou dans une affectation
  Returns:
    Un nouvel AstTds.affectable
  Exceptions:
    - IdentifiantNonDeclare
    - MauvaiseUtilisationIdentifiant
*)
let rec analyse_tds_affectable tds a modif = 
  match a with
  | AstSyntax.Dref new_aff ->
    let x = analyse_tds_affectable tds new_aff true in
    AstTds.Dref(x)
    
  | AstSyntax.Ident n ->
    begin
      match (chercherGlobalement tds n) with
      | None -> raise (IdentifiantNonDeclare n)
      | Some info ->
        begin
          match (info_ast_to_info info) with
          | InfoVar _ -> AstTds.Ident(info)
          | InfoConst _ ->
            begin
              if modif then 
                failwith "Internal Error"
              else
                AstTds.Ident(info)
            end
          | InfoFun _ -> (* On ne peut pas utiliser une fonction comme affectable *)
            raise (MauvaiseUtilisationIdentifiant(n))
          | _ -> failwith "Internal Error"
        end
    end
    
  | AstSyntax.Attribut(aff, str) ->
    (*
      extraction_info:
        affectable -> info_ast
      Description:
        Extrait l'info d'un affectable
      Parameters:
        - aff : Affectable dont on veut extraire l'info
      Returns:
        L'info de l'affectable
    *)
    let rec extraction_info aff =
      match aff with
      | Dref(aff2) -> extraction_info aff2
      | Ident(info) -> info
      | Attribut(_, info) -> info
    in
    
    let n_aff = analyse_tds_affectable tds aff modif in
    let info = extraction_info n_aff in
    match info_ast_to_info info with
    | InfoVar(_, t, _, _) ->
      begin
        match t with
        | Struct(l_att) ->
          let t = List.fold_right (fun (t, n) res -> if str = n then t else res) l_att Undefined in
          if t != Undefined then
            AstTds.Attribut(n_aff, info_to_info_ast (InfoVar(str, t, 0, "")))
          else (* L'attribut ne correspond à aucun attribut du struct *)
            raise (MauvaiseUtilisationIdentifiant str)
        | _ -> (* Il faut que l'objet sur le quel on appelle l'attribut soit un struct *)
          raise (MauvaiseUtilisationIdentifiant str)
      end (* Ce ne peut etre qu'un InfoVar *)
    | _ -> raise (MauvaiseUtilisationIdentifiant str)


(*
  analyse_tds_affectable:
    tds -> AstSyntax.expression -> AstTds.expression
  Description:
    Vérifie la bonne utilisation des identifiants et transforme les expressions de l'AstSyntax en AstTds
  Parameters:
    - tds : la table des symboles courante
    - e : l'expression à analyser
  Returns:
    Une nouvelle AstTds.expression
  Exceptions:
    - IdentifiantNonDeclare
    - MauvaiseUtilisationIdentifiant
*)
let rec analyse_tds_expression tds e =
  match e with
  | AstSyntax.AppelFonction(str, l_expr) ->
    begin
      match chercherGlobalement tds str with
      
      | None -> (* L'identifiant est déja déclaré *)
        raise (IdentifiantNonDeclare str)

      | Some info -> 
        match (info_ast_to_info info) with
        | InfoFun(_, _, _) ->
          let nl_expr = List.map (fun e -> analyse_tds_expression tds e) l_expr in
          AstTds.AppelFonction(info, nl_expr)

        | _ -> (* L'identifiant ne fait pas référence à une fonction, on lève une exception *)
          raise (MauvaiseUtilisationIdentifiant str)
    end

  | AstSyntax.Booleen(b) -> AstTds.Booleen(b)
  
  | AstSyntax.Entier(i) -> AstTds.Entier(i)
  
  | AstSyntax.Unaire(u, expr) ->
    let new_expr = analyse_tds_expression tds expr in
    AstTds.Unaire (u, new_expr)
  
  | AstSyntax.Binaire(b, expr_1, expr_2) ->
    let new_expr_1 = analyse_tds_expression tds expr_1 in
    let new_expr_2 = analyse_tds_expression tds expr_2 in
    AstTds.Binaire(b, new_expr_1, new_expr_2)
  
  | AstSyntax.Affectable(a) -> AstTds.Affectable(analyse_tds_affectable tds a false)
  
  | AstSyntax.Null -> AstTds.Null
  
  | AstSyntax.NewType(t) -> AstTds.NewType(t)
  
  | AstSyntax.Adresse(n) ->
    let info = chercherGlobalement tds n in
    begin
      match info with
      | None ->
        raise (IdentifiantNonDeclare n)
      | Some i ->
        begin
          match (info_ast_to_info i) with
          | InfoVar _ -> AstTds.Adresse(i)
          | _ -> raise (MauvaiseUtilisationIdentifiant(n))
        end
    end
  
  | AstSyntax.Tuple(le) ->
    let n_le = List.map (fun e -> analyse_tds_expression tds e) le in
    AstTds.Tuple(n_le)


(*
  analyse_tds_typedef:
    tds -> AstSyntax.typedef -> AstTds.instruction
  Description:
    Transforme les typedef de l'AstSyntax en AstTds.instructions
  Parameters:
    - tds : la table des symboles courante
    - td : le typedef à analyser
  Returns:
    Une nouvelle AstTds.affectable
  Exceptions:
    - DoubleDeclaration
*)
let analyse_tds_typedef tds (AstSyntax.TypeDef(n, t)) =
  begin
    match chercherLocalement tds n with
    | None -> (* L'identifiant n'est pas trouvé dans la tds locale du bloc, on ajoute donc dans la tds la constante *)
      ajouter tds n (info_to_info_ast (InfoType (n, t)));
      Empty
    
    | Some _ -> (* L'identifiant est trouvé dans la tds locale, il a donc double déclaration *) 
      raise (DoubleDeclaration n)
  end


(*
  analyse_tds_instruction:
    tds -> AstSyntax.instruction -> AstTds.instruction
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme l'instruction en une instruction de type AstTds.instruction
  Parameters:
    - tds : la table des symboles courante
    - i : l'instruction à analyser
  Returns:
    Une nouvelle AstTds.instruction
  Exceptions:
    - DoubleDeclaration
    - IdentifiantNonDeclare
    - MauvaiseUtilisationIdentifiant
*)
let rec analyse_tds_instruction tds i =
  match i with
  | AstSyntax.Declaration (t, n, e) ->
    begin
      match chercherLocalement tds n with
      | None -> (* L'identifiant n'est pas trouvé dans la tds locale, il n'a donc pas été déclaré dans le bloc courant *)
          let nt = recherche_type tds t in
          
          (* Vérification de la bonne utilisation des identifiants dans l'expression et obtention de l'expression transformée *) 
          let ne = analyse_tds_expression tds e in
          
          (* Création de l'information associée à l'identfiant *)
          let info = InfoVar (n, nt, 0, "") in
          
          (* Création du pointeur sur l'information *)
          let ia = info_to_info_ast info in
          
          (* Ajout de l'information (pointeur) dans la tds *)
          ajouter tds n ia;
          
          (* Renvoie de la nouvelle déclaration où le nom a été remplacé par l'information et l'expression remplacée par l'expression issue de l'analyse *)
          AstTds.Declaration (nt, ia, ne) 
      
      | Some _ -> (* L'identifiant est trouvé dans la tds locale, il a donc déjà été déclaré dans le bloc courant *) 
          raise (DoubleDeclaration n)
    end
    
  | AstSyntax.Affectation (aff, e) ->
    begin
      let ne = analyse_tds_expression tds e in
      let n_a = analyse_tds_affectable tds aff false in
      match aff with
        | Dref _ ->
          Affectation (n_a, ne)
          
        | Ident(n) ->
          begin
            match chercherGlobalement tds n with
            | None -> (* L'identifiant n'est pas trouvé dans la tds globale. *) 
              raise (IdentifiantNonDeclare n)
            
            | Some info -> (* L'identifiant est trouvé dans la tds globale, il a donc déjà été déclaré. L'information associée est récupérée. *) 
              begin
                match info_ast_to_info info with
                | InfoVar _ -> (* Renvoie de la nouvelle affectation où le nom a été remplacé par l'information et l'expression remplacée par l'expression issue de l'analyse *)
                  Affectation (n_a, ne)
                
                | _ -> (* Modification d'une constante ou d'une fonction *)  
                  raise (MauvaiseUtilisationIdentifiant n) 
              end
          end
      | Attribut(_, _) -> (* RAF *)
        Affectation(n_a, ne)
    end
    
  | AstSyntax.Constante (n,v) -> 
      begin
        match chercherLocalement tds n with
        | None -> (* L'identifiant n'est pas trouvé dans la tds locale, il n'a donc pas été déclaré dans le bloc courant *)
          
          (* Ajout dans la tds de la constante *)
          ajouter tds n (info_to_info_ast (InfoConst (n,v))); 
          
          (* Suppression du noeud de déclaration des constantes devenu inutile *)
          Empty
        
        | Some _ -> (* L'identifiant est trouvé dans la tds locale, il a donc déjà été déclaré dans le bloc courant *) 
          raise (DoubleDeclaration n)
      end
  
  | AstSyntax.Affichage e -> 
      (* Vérification de la bonne utilisation des identifiants dans l'expression et obtention de l'expression transformée *)
      let ne = analyse_tds_expression tds e in
      
      (* Renvoie du nouvel affichage où l'expression remplacée par l'expression issue de l'analyse *)
      Affichage (ne)
  
  | AstSyntax.Conditionnelle (c,t,e) -> 
      (* Analyse de la condition *)
      let nc = analyse_tds_expression tds c in
      
      (* Analyse du bloc then *)
      let tast = analyse_tds_bloc tds t in
      
      (* Analyse du bloc else *)
      let east = analyse_tds_bloc tds e in
      
      (* Renvoie la nouvelle structure de la conditionnelle *)
      Conditionnelle (nc, tast, east)
  
  | AstSyntax.TantQue (c,b) -> 
      (* Analyse de la condition *)
      let nc = analyse_tds_expression tds c in
      
      (* Analyse du bloc *)
      let bast = analyse_tds_bloc tds b in
      
      (* Renvoie la nouvelle structure de la boucle *)
      TantQue (nc, bast)
  
  | AstSyntax.Retour (e) -> 
      (* Analyse de l'expression *)
      let ne = analyse_tds_expression tds e in
      Retour (ne)
  
  | AstSyntax.LocalTypeDef(td) ->
    (* Analyse du typedef *)
    analyse_tds_typedef tds td


(*
  analyse_tds_bloc:
    tds -> AstSyntax.bloc -> AstTds.bloc
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme le bloc en un bloc de type AstTds.bloc
  Parameters:
    - tds : la table des symboles courante
    - li : la liste d'instructions (bloc) à analyser
  Returns:
    Un nouvel AstTds.bloc
*)
and analyse_tds_bloc tds li =
    (* Entrée dans un nouveau bloc, donc création d'une nouvelle tds locale pointant sur la table du bloc parent *)
    let tdsbloc = creerTDSFille tds in
    
    (* Analyse des instructions du bloc avec la tds du nouveau bloc, cette tds est modifiée par effet de bord *)
    let nli = List.map (analyse_tds_instruction tdsbloc) li in
    
    (* afficher_locale tdsbloc ; *) (* décommenter pour afficher la table locale *)
    nli


(*
  analyse_tds_fonction:
    tds -> AstSyntax.fonction -> AstTds.fonction
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme la fonction en une fonction de type AstTds.fonction
  Parameters:
    - maintds : la table des symboles courante
    - f : la fonction à analyser
  Returns:
    Une nouvelle AstTds.instruction
  Exceptions:
    - DoubleDeclaration
*)
let analyse_tds_fonction maintds (AstSyntax.Fonction(t, str, l_typstr, bloc))  =
  begin
    match chercherLocalement maintds str with
    | Some _ -> (* L'identifiant est trouvé dans la tds locale, il a donc déjà été déclaré dans le bloc courant *)
      raise (DoubleDeclaration str)       
    
    | None -> 
      begin
        (* Copie de la tds globale dans la tds locale au bloc *)
        let tds_bloc = creerTDSFille maintds in

        (* Ajouter les arguments de la fonction dans la tds locale *)
        let nl_typinfo = (List.map (
          fun (t, nom) ->
            let nt = recherche_type maintds t in
            match chercherLocalement tds_bloc nom with
            | None ->
              let i_ast_var = info_to_info_ast (Tds.InfoVar(nom, nt, 0, "")) in
              ajouter tds_bloc nom i_ast_var;
              (nt, i_ast_var)
            (* Si un argument est en double, on lève une exception *)
            | Some _ -> raise (DoubleDeclaration nom) 
        ) l_typstr) in

        let nt = recherche_type maintds t in

        (* On crée l'info de la fonction *)
        let info_fun = InfoFun(str, nt, (List.map (fun (t, _) -> recherche_type maintds t) l_typstr)) in
        
        (* On ajoute a la tds locale la fonction pour qu'il puisse y avoir des appels récursifs *)
        let _ = ajouter tds_bloc str (info_to_info_ast info_fun) in

        (* On génère le nouveau bloc avec la tds locale *)
        let new_bloc = analyse_tds_bloc tds_bloc bloc in

        (* On ajoute la fonction a la tds globale *)
        ajouter maintds str (info_to_info_ast info_fun);

        (* On retourne la AstTds fonction *)
        AstTds.Fonction(nt, (info_to_info_ast info_fun), nl_typinfo, new_bloc)
      end
  end


(*
  analyser:
    AstSyntax.ast -> AstTds.ast
  Description:
    Vérifie la bonne utilisation des identifiants et tranforme le programme en un programme de type AstTds.ast
  Parameters:
    - p : le programme à analyser
  Returns:
    L'AstTds correspondant au programme
*)
let analyser (AstSyntax.Programme (typedefs,fonctions,prog)) =
  let tds = creerTDSMere () in
  let _ = List.map (analyse_tds_typedef tds) typedefs in
  let nf = List.map (analyse_tds_fonction tds) fonctions in 
  let nb = analyse_tds_bloc tds prog in
  AstTds.Programme (nf,nb)

end
