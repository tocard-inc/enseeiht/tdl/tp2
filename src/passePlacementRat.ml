(* Module de la passe de typage *)
module PassePlacementRat : Passe.Passe with type t1 = Ast.AstType.programme and type t2 = Ast.AstPlacement.programme =
struct

  open Tds
  open Ast
  open AstPlacement
  open Type

  type t1 = Ast.AstType.programme
  type t2 = Ast.AstPlacement.programme


(*
  analyse_placement_instruction:
    instruction -> int -> string -> int
  Description:
    Modifie le placement des info (base et registre) et retourne le nombre de place prise
  Parameters:
    - i : l'instruction à analyser
    - base : la base actuel
    - reg : le registre actuel
  Returns:
    La place prise par l'instruction
*)
let rec analyse_placement_instruction i base reg =
  match i with
  | AstType.Declaration(info, _) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, t, _, _) ->
        let taille = getTaille t in
        modifier_adresse_info base reg info;
        taille
      | _ -> failwith "Internal Error"
    end

  | AstType.Conditionnelle(_, b1, b2) ->
    let _ = analyse_placement_bloc base reg b1 in
    let _ = analyse_placement_bloc base reg b2 in
    0

  | AstType.TantQue(_, b) ->
    let _ = analyse_placement_bloc base reg b in
    0

  | _ -> 0


(*
  analyse_placement_bloc:
    int -> string -> AstType.bloc -> int
  Description:
    Analyse le bloc instruction par instruction (base et registre) et retourne le nombre de place prise par le bloc
  Parameters:
    - base : la base actuel
    - reg : le registre actuel
    - bloc : bloc à analyser
  Returns:
    La place prise en mémoire par le bloc
*)
and analyse_placement_bloc base reg bloc =
  match bloc with
  | [] -> base
  | t::q ->
    let taille = analyse_placement_instruction t base reg in
    analyse_placement_bloc (base + taille) reg q


(*
  analyse_placement_fonction:
    AstType.fonction -> AstPlacement.fonction
  Description:
    Analyse le placement d'une fonction
  Parameters:
    - fonction : La fonction à analyser
  Returns:
    La fonction analysée
*)
and analyse_placement_fonction (AstType.Fonction(info, l_typinfo, bloc)) =
    let _ = List.fold_right (fun x res_q ->
      begin
        match info_ast_to_info x with
        | InfoVar(_,t,_,_) ->
          modifier_adresse_info (res_q - (getTaille t)) "LB" x;
          res_q - (getTaille t)
        | _ -> failwith "Internal Error"
      end
    ) l_typinfo 0 in
    
    (* Avec les 3 places prises par l'enregistrement d'activation, l'analyse du bloc commence à 3 *)
    let _ = analyse_placement_bloc 3 "LB" bloc in
    Fonction(info, l_typinfo, bloc)


(*
  analyser:
    AstType -> AstPlacement
  Description:
    Analyser un programme
  Parameters:
    - programme : Le programme à analyser
  Returns:
    Le programme analysée
*)
let analyser (AstType.Programme(fonctions, prog)) =
  let n_fonctions = List.map analyse_placement_fonction fonctions in
  let _ = analyse_placement_bloc 0 "SB" prog in
  Programme(n_fonctions, prog)
end
