/* Imports. */

%{
open Type
open Ast.AstSyntax
%}

%token <int> ENTIER
%token <string> ID
%token <string> TID
%token RETURN
%token PV
%token AO
%token AF
%token PF
%token PO
%token EQUAL
%token CONST
%token PRINT
%token IF
%token ELSE
%token WHILE
%token BOOL
%token INT
%token RAT
%token CALL 
%token CO
%token CF
%token SLASH
%token NUM
%token DENOM
%token TRUE
%token FALSE
%token PLUS
%token MULT
%token INF
%token EOF

%token NULL
%token NEW
%token AMP
%token TYPEDEF
%token PT
%token STRUCT

(* Type de l'attribut synthétisé des non-terminaux *)
%type <programme> prog
%type <instruction list> bloc
%type <fonction> fonc
%type <instruction list> is
%type <instruction> i
%type <typ> typ
%type <(typ*string) list> dp
%type <expression> e 
%type <expression list> cp

(* Type et définition de l'axiome *)
%start <Ast.AstSyntax.programme> main

%%

main : lfi=prog EOF     {lfi}

prog :
| ltd=td lf=fonc lfi=prog   {let (Programme (_,    lf1, li))=lfi in (Programme (ltd,      lf::lf1,li))}
| ID li=bloc                {Programme ([], [], li)}

td :
|                                             {[]}
| TYPEDEF tid=TID EQUAL type1=typ PV td1=td   {(TypeDef(tid, type1))::td1}

fonc :
| t=typ n=ID PO p=dp PF AO li=is AF {Fonction(t,n,p,li)}

bloc : AO li = is AF      {li}

is :
|                 {[]}
| i1=i li=is      {i1::li}

i :
| t=typ n=ID EQUAL exp=e PV           {Declaration (t,n,exp)}
| n=ID EQUAL exp=e PV                 {Affectation (Ident(n),exp)}
| CONST n=ID EQUAL e=ENTIER PV        {Constante (n,e)}
| PRINT exp=e PV                      {Affichage (exp)}
| IF exp=e li1=bloc ELSE li2=bloc     {Conditionnelle (exp,li1,li2)}
| WHILE exp=e li=bloc                 {TantQue (exp,li)}
| RETURN exp=e PV                     {Retour (exp)}
| aff=a EQUAL exp=e PV                {Affectation (aff, exp)}
| aff=a PLUS EQUAL exp=e PV           {Affectation (aff, Binaire (Plus, Affectable(aff), exp))}
| TYPEDEF tid=TID EQUAL type1=typ PV  {LocalTypeDef (TypeDef(tid, type1))}

dp :
|                     {[]}
| t=typ n=ID lp=dp    {(t,n)::lp}

typ :
| BOOL              {Bool}
| INT               {Int}
| RAT               {Rat}
| t1=typ MULT       {Pointeur (t1)}
| t1=TID            {TIdent (t1)}
| STRUCT AO p=dp AF {Struct (p)}

e : 
| CALL n=ID PO lp=cp PF   {AppelFonction (n,lp)}
| CO e1=e SLASH e2=e CF   {Binaire(Fraction,e1,e2)}
| TRUE                    {Booleen true}
| FALSE                   {Booleen false}
| e=ENTIER                {Entier e}
| NUM e1=e                {Unaire(Numerateur,e1)}
| DENOM e1=e              {Unaire(Denominateur,e1)}
| PO e1=e PLUS e2=e PF    {Binaire (Plus,e1,e2)}
| PO e1=e MULT e2=e PF    {Binaire (Mult,e1,e2)}
| PO e1=e EQUAL e2=e PF   {Binaire (Equ,e1,e2)}
| PO e1=e INF e2=e PF     {Binaire (Inf,e1,e2)}
| PO exp=e PF             {exp}
| a1=a                    {Affectable a1}
| NULL                    {Null}
| PO NEW t1=typ PF        {NewType (t1)}
| AMP n=ID                {Adresse (n)}
| AO c1=cp AF             {Tuple(c1)}

cp :
|               {[]}
| e1=e le=cp    {e1::le}

a :
| n=ID                {Ident (n)}
| PO MULT a1=a PF     {Dref (a1)}
| PO aff=a PT n=ID PF {Attribut (aff, n)}