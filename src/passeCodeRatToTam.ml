(* Module de la passe de typage *)
module PasseCodeRatToTam : Passe.Passe with type t1 = Ast.AstPlacement.programme and type t2 = string =
struct

  open Tds
  open Ast
  open AstType
  open Type
  open Code

  type t1 = Ast.AstPlacement.programme
  type t2 = string

(*
  getTailleAvant:
    affectable -> t2 -> int
  Description:
    Avoir la taille avant l'attribut n dans un struct
  Parameters:
    - aff : affectable de type struct
    - n : nom de l'attribut
  Returns:
    La taille prise par les attribut avant n
*)
let rec getTailleAvant aff n =
  match aff with
  | Dref(aff2, _) -> getTailleAvant aff2 n
  
  | Ident(info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, Struct(l_typstr), _, _) ->
        let tai, _ = List.fold_left (
          fun (res, trouv) (t, s) ->
            if s = n || trouv then
              (res, true)
            else
              (res + (getTaille t), false)
        ) (0, false) l_typstr
        in tai
      | _ -> failwith "Internal Error"
    end
  
  | Attribut(aff2, info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, _, _, _) ->
        let t2 = getType aff2 in
        let aff_gen = Ident (info_to_info_ast (InfoVar("", t2, 0, ""))) in
        getTailleAvant aff_gen n
      | _ -> failwith "Internal Error"
    end

(*
  getType:
    affectable -> typ
  Description:
    Avoir le type d'un affectable
  Parameters:
    - aff : affectable dont on veut avoir le type
  Returns:
    Le type de l'affectable
*)
and getType aff =
  match aff with
  | Dref(aff2, _) -> getType aff2
  
  | Ident(info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, t, _, _) -> t
      | _ -> failwith "Internal Error"
    end
  
  | Attribut(aff2, info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(n, _, _, _) ->
        begin
          match getType aff2 with
          | Struct(l_typstr) ->
            let t2 = List.fold_left (
              fun res (t, s) ->
                if s = n then
                  t
                else
                  res
            ) Undefined l_typstr
            in t2
          | _ -> failwith "Internal Error"
        end
      | _ -> failwith "Internal Error"
    end

(*
  getTailleAttribut:
    affectable -> t2 -> int
  Description:
    Avoir la taille d'un attribut d'un affectable de type struct
  Parameters:
    - aff : affectable de l'attribut
    - n : nom de l'attribut
  Returns:
    La taille de l'attribut
*)
and getTailleAttribut aff n =
  match aff with
  | Dref(aff2, _) -> getTailleAttribut aff2 n
  
  | Ident(info) ->
    begin
      match info_ast_to_info info with
      | InfoVar(_, Struct(l_typstr), _, _) ->
        List.fold_right (
          fun (t, s) res ->
            if s = n then
              getTaille t
            else
              res
        ) l_typstr 0
      | _ -> failwith "Internal Error"
    end
  
  | Attribut(_, _) -> getTaille (getType aff)

(*
  getBase:
    affectable -> int
  Description:
    Avoir la base d'un affectable
  Parameters:
    - aff : affectable dont on veut avoir la base
  Returns:
    La base de l'affectable
*)
and getBase aff =
  match aff with
  | Dref (a,_) -> getBase a
  
  | Ident(i) ->
    begin
      match info_ast_to_info i with
      | InfoVar(_, _, base, _) -> base
      | _ -> failwith "Internal Error"
    end
  
  | Attribut(a,i) ->
    begin
      match info_ast_to_info i with
      | InfoVar (n,_,_,_) ->
        let ib = getBase a in
        let ita = getTailleAvant aff n in
        let res = ib + ita in
        res
      | _ -> failwith "Internal Error"
    end

(*
  getBase:
    affectable -> t2
  Description:
    Avoir le registre d'un affectable
  Parameters:
    - aff : affectable dont on veut avoir le registre
  Returns:
    Le registre de l'affectable
*)
and getReg aff =
  match aff with
  | Dref (a,_) -> getReg a
  
  | Ident(i) ->
    begin
      match info_ast_to_info i with
      | InfoVar(_, _, _, reg) -> reg
      | _ -> failwith "Internal Error"
    end
  
  | Attribut(a,_) -> getReg a

(*
  extraction_info:
    affectable -> info_ast
  Description:
    Avoir l'info d'un affectable
  Parameters:
    - aff : affectable dont on veut avoir l'info
  Returns:
    L'info de l'affectable
*)
and extraction_info aff =
  match aff with
  | Dref(aff2, _) -> extraction_info aff2
  | Ident(info) -> info
  | Attribut(aff2, _) -> extraction_info aff2

(*
  analyse_code_affectable:
    affectable -> t2
  Description:
    Convertir un affectable en code TAM
  Parameters:
    - aff : affectable que l'on veut convertir
  Returns:
    Le code TAM de l'affectable
*)
and analyse_code_affectable a =
  match a with
  | Dref(aff, t) ->
    analyse_code_affectable aff ^
    "LOADI (" ^ (string_of_int (getTaille t)) ^ ")\n"

  | Ident(i) ->
    begin
      match info_ast_to_info i with
      | InfoVar(_, t, base, reg) -> "LOAD (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int base) ^ "[" ^ reg ^ "]\n"
      | InfoConst(_,v) -> "LOADL " ^ (string_of_int v) ^ "\n"
      | _ -> failwith "Internal Error"
    end

  | Attribut(_, _) ->
    let base = getBase a in
    let reg = getReg a in
    "LOAD (" ^ (string_of_int (getTaille (getType a))) ^ ") " ^ (string_of_int (base)) ^ "[" ^ reg ^ "]\n"

(*
  analyse_code_expression:
    expression -> t2
  Description:
    Convertir une expression en code TAM
  Parameters:
    - e : expression que l'on veut convertir
  Returns:
    Le code TAM correspondant
*)
and analyse_code_expression e =
  match e with
  | AppelFonction(i, le) ->
    begin
      match info_ast_to_info i with
      | InfoFun(nom,_,_) ->
        (List.fold_right (fun e res -> analyse_code_expression e ^ res) le "") ^
        "CALL (ST) " ^ nom ^ "\n"
      | _ -> failwith "Internal Error"
    end

  | Booleen(b) ->
    if b then
      "LOADL 1\n"
    else
      "LOADL 0\n"

  | Entier(n) ->
    "LOADL " ^ (string_of_int n) ^ "\n"

  | Unaire(u, e) ->
    (analyse_code_expression e) ^
    begin
      match u with
      | Numerateur -> "POP (0) 1\n"
      | Denominateur -> "POP (1) 1\n"
    end

  | Binaire(b, e1, e2) ->
    (analyse_code_expression e1) ^
    (analyse_code_expression e2) ^
    begin
      match b with
      | Fraction -> "CALL (ST) norm\n"
      | PlusInt -> "SUBR IAdd\n"
      | MultInt -> "SUBR IMul\n"
      | EquInt -> "SUBR IEq\n"
      | Inf -> "SUBR ILss\n"
      | PlusRat -> "CALL (ST) RAdd\n"
      | MultRat -> "CALL (ST) RMul\n"
      | EquBool -> "SUBR IEq\n"
    end

  | Affectable(a) -> analyse_code_affectable a

  | Null -> "SUBR MVoid\n"

  | NewType(t) ->
    "LOADL " ^ (string_of_int (getTaille t)) ^ "\n" ^
    "SUBR MAlloc\n"

  | Adresse(i) ->
    begin
      match info_ast_to_info i with
      | InfoVar(_, _, base, _) -> "LOADL " ^ (string_of_int base) ^ "\n"
      | _ -> failwith "Internal Error"
    end
  | Tuple(l_expr) ->
    List.fold_right (fun e res -> analyse_code_expression e ^ res) l_expr ""
  
  | Nothing -> ""

(*
  analyse_code_instruction:
    instruction -> t2 -> t2 -> t2 -> t2
  Description:
    Convertir une instruction en code TAM
  Parameters:
    - i : instruction que l'on veut ecrire en TAM
    - taille_return : taille du retour si on est dans une fonction
    - taille_args : taille des arguments si on est dans une fonction
    - taille_var : taille des varibales local a une fonction ou a un bloc
  Returns:
    Le code TAM correpondant à l'instruction
*)
and analyse_code_instruction i taille_return taille_args taille_var =
  match i with
  | Declaration(i, e) ->
    begin
      match info_ast_to_info i with
      | InfoVar(_, t, base, reg) ->
        "PUSH " ^ (string_of_int (getTaille t)) ^ "\n" ^
        analyse_code_expression e ^
        "STORE (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int base) ^ "[" ^ reg ^ "]\n"
      | _ -> failwith "Internal Error"
    end

  | Affectation(a, e) ->
    begin
      match a with
      | Ident(i) ->
        begin
          match (info_ast_to_info i) with
          | InfoVar (_, t, base, reg) ->
            (analyse_code_expression e) ^
            "STORE (" ^ string_of_int (getTaille t) ^ ") " ^ (string_of_int base) ^ "[" ^ reg ^ "]\n"
          | _ -> failwith "Internal Error"
        end

      | Dref(a, t) ->
        (analyse_code_expression e) ^
        (analyse_code_affectable a) ^
        "STOREI (" ^ string_of_int (getTaille t) ^ ")\n"

      | Attribut(aff, info) ->
        begin
          match (info_ast_to_info (extraction_info aff)) with
          | InfoVar(_, _, _, reg) ->
            begin
              match (info_ast_to_info info) with
              | InfoVar(n, _, _, _) ->
                (analyse_code_expression e) ^
                "STORE (" ^ (string_of_int (getTailleAttribut a n)) ^ ") " ^ (string_of_int (getBase a)) ^ "[" ^ reg ^ "]\n"
              | _ -> failwith "Internal Error"
            end
          | _ -> failwith "Internal Error"
        end
    end


  | AffichageInt(e) ->
    (analyse_code_expression e) ^
    "SUBR IOut\n"

  | AffichageRat(e) ->
    (analyse_code_expression e) ^
    "CALL (ST) ROut\n"

  | AffichageBool(e) ->
    (analyse_code_expression e) ^
    "SUBR BOut\n"

  | AffichagePointeur(e) ->
    "LOADL '@'\n" ^ 
    "SUBR COut\n" ^
    (analyse_code_expression e) ^
    "SUBR IOut\n"

  | AffichageStruct(e, t) ->
    let rec affichage base reg t0 =
      match t0 with
      | Struct(l_typstr) ->
        begin
          let l_t, _ = List.split l_typstr in
          let res, _ = List.fold_left (
            fun (res, taille) t ->
              match t with
              | Bool -> (
                res ^
                "LOAD (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int taille) ^ "[" ^ reg ^ "]\n" ^
                "SUBR BOut\n" ^
                "LOADL ' '\n" ^
                "SUBR COut\n"
                ,taille + (getTaille t))
              
              | Int -> (
                res ^
                "LOAD (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int taille) ^ "[" ^ reg ^ "]\n" ^
                "SUBR IOut\n" ^
                "LOADL ' '\n" ^
                "SUBR COut\n"
                ,taille + (getTaille t))
              
              | Rat -> (
                res ^
                "LOAD (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int taille) ^ "[" ^ reg ^ "]\n" ^
                "CALL (ST) ROut\n" ^
                "LOADL ' '\n" ^
                "SUBR COut\n"
                ,taille + (getTaille t))
              
              | Pointeur(_) -> (
                res ^
                "LOADL '@'\n" ^ 
                "SUBR COut\n" ^
                "LOAD (" ^ (string_of_int (getTaille t)) ^ ") " ^ (string_of_int taille) ^ "[" ^ reg ^ "]\n" ^
                "SUBR IOut\n" ^
                "LOADL ' '\n" ^
                "SUBR COut\n"
                ,taille + (getTaille t))
              
              | Struct(_) -> (
                res ^
                "LOADL '{'\n" ^ 
                "SUBR COut\n" ^
                (affichage taille reg t) ^
                "LOADL '}'\n" ^ 
                "SUBR COut\n" ^
                "LOADL ' '\n" ^
                "SUBR COut\n"
                ,taille + (getTaille t))
              
              | _ -> failwith "Internal Error"
              
          ) ("LOADL ' '\nSUBR COut\n", base) l_t in
          res
        end
      | _ -> failwith "Internal Error"
    in
    
    let affichage_expression e t =
      let l_typstr =
        match t with
        | Struct(l_typstr) -> l_typstr
        | _ -> failwith "Internal Error"
      in
      let l_t, _ = List.split l_typstr in
      match e with
      | Tuple(l_e) ->
        List.fold_right (
          fun (e, t) res ->
          let i =
            match t with
            | Bool -> AffichageBool(e)
            | Int -> AffichageInt(e)
            | Rat -> AffichageRat(e)
            | Pointeur(_) -> AffichagePointeur(e)
            | Struct(_) -> AffichageStruct(e, t)
            | _ -> failwith "Internal Error"
          in
          "LOADL ' '\n" ^ 
          "SUBR COut\n" ^
          (analyse_code_instruction i "0" "0" "0") ^
          res
        ) (List.combine l_e l_t) "LOADL ' '\nSUBR COut\n"

      | Affectable(a) ->
        begin
          match a with
          | Dref(_, _) -> failwith "Internal Error"
          
          | Ident(i) ->
            begin
              match info_ast_to_info i with
              | InfoVar(_, t, base, reg) ->
                affichage base reg t
              | _ -> failwith "Internal Error"
            end
          
          | Attribut(_, i) ->
            match info_ast_to_info i with
            | InfoVar(_, t, _, _) ->
              let base = getBase a in
              let reg = getReg a in
              affichage base reg t
            | _ -> failwith "Internal Error"
        end
      
      | AppelFonction(i, le) ->
        begin
          match info_ast_to_info i with
          | InfoFun(nom, Struct(l_typstr), _) ->
            let t = Struct(l_typstr) in
            let base = -getTaille t in
            let reg = "ST" in
            (List.fold_right (fun e res -> analyse_code_expression e ^ res) le "") ^
            "CALL (ST) " ^ nom ^ "\n" ^
            (affichage base reg t) ^
            "POP (0) " ^ (string_of_int (getTaille t)) ^ "\n"
          | _ -> failwith "Internal Error"
        end
      | _ -> failwith "Internal Error"
    in

    "LOADL '{'\n" ^ 
    "SUBR COut\n" ^
    (affichage_expression e t) ^
    "LOADL '}'\n" ^ 
    "SUBR COut\n"

  | Conditionnelle(e, b1, b2) ->
    let etiq1 = getEtiquette () in
    let etiq2 = getEtiquette () in
    (analyse_code_expression e) ^
    "JUMPIF (0) " ^ etiq1 ^ "\n" ^
    (analyse_code_bloc b1 taille_return taille_args) ^
    "JUMP " ^ etiq2 ^ "\n" ^
    etiq1 ^ "\n" ^
    (analyse_code_bloc b2 taille_return taille_args) ^
    etiq2 ^ "\n"

  | TantQue(e, b) ->
    let etiq_while = getEtiquette () in
    let etiq_fin = getEtiquette () in
    etiq_while ^ "\n" ^
    (analyse_code_expression e) ^
    "JUMPIF (0) " ^ etiq_fin ^ "\n" ^
    (analyse_code_bloc b taille_return taille_args) ^
    "JUMP " ^ etiq_while ^ "\n" ^
    etiq_fin ^ "\n"

  | Retour(e) ->
    (analyse_code_expression e) ^
    "POP (" ^ taille_return ^ ") " ^ taille_var ^ "\n" ^
    "RETURN (" ^ taille_return ^ ") " ^ taille_args ^ "\n"

  | Empty -> ""

(*
  analyse_code_bloc:
    bloc -> t2 -> t2 -> t2
  Description:
    Convertir un bloc en code TAM
  Parameters:
    - bloc : bloc que l'on veut ecrire en TAM
    - taille_return : taille du retour si on est dans une fonction
    - taille_args : taille des arguments si on est dans une fonction
  Returns:
    Le code TAM correpondant au bloc
*)
and analyse_code_bloc bloc taille_return taille_args =
  let taille_var = string_of_int (List.fold_right (fun e res -> taille_variables_declarees e + res) bloc 0) in
  List.fold_right (
    fun i res -> (analyse_code_instruction i taille_return taille_args taille_var) ^ res
  ) bloc "" ^
  "POP (0) " ^ taille_var ^ "\n"

(*
  analyse_code_fonction:
    fonction -> t2 -> t2 -> t2
  Description:
    Convertir une fonction en code TAM
  Parameters:
    - fonction : fonction à convertir
  Returns:
    Le code TAM de la fonction
*)
and analyse_code_fonction (AstPlacement.Fonction(info, _, bloc)) =
  match info_ast_to_info info with
  | InfoFun(name, t, l_t) ->
    let taille_return = string_of_int (getTaille t) in
    let taille_args = string_of_int (List.fold_right (fun e res -> getTaille e + res) l_t 0) in
    name ^ "\n" ^
    (analyse_code_bloc bloc taille_return taille_args) ^
    "HALT\n"

  | _ -> failwith "Internal Error"

(*
  analyse_code_fonction:
    prgramme -> t2
  Description:
    Convertir un programme en code TAM
  Parameters:
    - programme : programme à convertir
  Returns:
    Le code TAM du programme
*)
let analyser (AstPlacement.Programme(fonctions, prog)) =
  let code = getEntete () in
  let code = code ^ List.fold_right (fun e res -> (analyse_code_fonction e) ^ res) fonctions "" in
  let code = code ^ "main\n" in
  let code = code ^ (analyse_code_bloc prog "0" "0") in
  code ^ "\nHALT"
end
